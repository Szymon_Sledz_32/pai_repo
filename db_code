create table aircraft_categories
(
    id   serial
        constraint aircraft_categories_pk
            primary key,
    name varchar(100) not null
);

alter table aircraft_categories
    owner to akxlgzthnxmupr;

create unique index aircraft_categories_id_uindex
    on aircraft_categories (id);

create unique index aircraft_categories_name_uindex
    on aircraft_categories (name);

create table license_types
(
    id   serial
        constraint license_types_pk
            primary key,
    name varchar(100) not null
);

alter table license_types
    owner to akxlgzthnxmupr;

create unique index license_types_id_uindex
    on license_types (id);

create unique index license_types_name_uindex
    on license_types (name);

create table countries
(
    id   serial
        constraint countries_pk
            primary key,
    name varchar(255) not null
);

alter table countries
    owner to akxlgzthnxmupr;

create unique index countries_id_uindex
    on countries (id);

create unique index countries_name_uindex
    on countries (name);

create table cities
(
    id   serial
        constraint cities_pk
            primary key,
    name varchar(255) not null
);

alter table cities
    owner to akxlgzthnxmupr;

create unique index cities_id_uindex
    on cities (id);

create unique index cities_name_uindex
    on cities (name);

create table streets
(
    id   serial
        constraint streets_pk
            primary key,
    name varchar(255) not null
);

alter table streets
    owner to akxlgzthnxmupr;

create unique index streets_id_uindex
    on streets (id);

create unique index streets_name_uindex
    on streets (name);

create table zip_codes
(
    id   serial
        constraint zip_codes_pk
            primary key,
    code varchar(10) not null
);

alter table zip_codes
    owner to akxlgzthnxmupr;

create unique index zip_codes_codes_uindex
    on zip_codes (code);

create unique index zip_codes_id_uindex
    on zip_codes (id);

create table states
(
    id   serial
        constraint states_pk
            primary key,
    name varchar(255) not null
);

alter table states
    owner to akxlgzthnxmupr;

create table addresses
(
    id           serial
        constraint addresses_pk
            primary key,
    id_country   integer not null
        constraint countries_addresses___fk
            references countries,
    id_city      integer not null
        constraint cities_addresses___fk
            references cities,
    id_zip_code  integer not null
        constraint zip_codes_addresses___fk
            references zip_codes,
    id_street    integer not null
        constraint streets_addresses___fk
            references streets,
    number       integer not null,
    local_number integer,
    id_state     integer not null
        constraint states_addresses___fk
            references states
);

alter table addresses
    owner to akxlgzthnxmupr;

create unique index addresses_id_uindex
    on addresses (id);

create unique index states_id_uindex
    on states (id);

create unique index states_name_uindex
    on states (name);

create table aircrafts_makes
(
    id   serial
        constraint aircrafts_makes_pk
            primary key,
    make varchar(255) not null
);

alter table aircrafts_makes
    owner to akxlgzthnxmupr;

create unique index aircrafts_makes_id_uindex
    on aircrafts_makes (id);

create unique index aircrafts_makes_make_uindex
    on aircrafts_makes (make);

create table aircrafts_models
(
    id    serial
        constraint aircrafts_models_pk
            primary key,
    model varchar(255) not null
);

alter table aircrafts_models
    owner to akxlgzthnxmupr;

create unique index aircrafts_models_id_uindex
    on aircrafts_models (id);

create unique index aircrafts_models_model_uindex
    on aircrafts_models (model);

create table aircrafts_landing_gears
(
    id           serial
        constraint aircrafts_landing_gears_pk
            primary key,
    landing_gear varchar(255) not null
);

alter table aircrafts_landing_gears
    owner to akxlgzthnxmupr;

create table aircraft_details
(
    id              serial
        constraint aircraft_details_pk
            primary key,
    id_make         integer not null
        constraint aircraft_makes__aircraft_details___fk
            references aircrafts_makes,
    id_model        integer not null
        constraint aircraft_models__aircraft_details___fk
            references aircrafts_models,
    id_landing_gear integer
        constraint aircraft_landing_gear__aircraft_details___fk
            references aircrafts_landing_gears,
    seats           integer,
    load            integer,
    hangared        boolean
);

alter table aircraft_details
    owner to akxlgzthnxmupr;

create table aircrafts
(
    id                   serial
        constraint aircrafts_pk
            primary key,
    year_of_production   integer not null,
    image                varchar,
    id_aircraft_category integer not null
        constraint aircraft_categories_aircrafts___fk
            references aircraft_categories
            on update cascade on delete cascade,
    id_aircraft_details  integer not null
        constraint aircraft_details__aircrafts___fk
            references aircraft_details
            on update cascade on delete cascade
);

alter table aircrafts
    owner to akxlgzthnxmupr;

create unique index aircrafts_id_uindex
    on aircrafts (id);

create table aircrafts_licenses
(
    id_aircraft integer not null
        constraint aircrafts_aircrafts_licenses___fk
            references aircrafts
            on update cascade on delete cascade,
    id_license  integer not null
        constraint licenses_aircrafts_licenses___fk
            references license_types
            on update cascade on delete cascade
);

alter table aircrafts_licenses
    owner to akxlgzthnxmupr;

create table aircraft_has_license_types
(
    id_aircraft     integer not null
        constraint aircrafts__aircraft_has_license_types___fk
            references aircrafts
            on update cascade on delete cascade,
    id_license_type integer not null
        constraint license_types__aircraft_has_license_types___fk
            references license_types
);

alter table aircraft_has_license_types
    owner to akxlgzthnxmupr;

create unique index aircraft_details_id_uindex
    on aircraft_details (id);

create unique index aircrafts_landing_gears_id_uindex
    on aircrafts_landing_gears (id);

create unique index aircrafts_landing_gears_landing_gear_uindex
    on aircrafts_landing_gears (landing_gear);

create table user_details
(
    id          serial
        constraint user_details_pk
            primary key,
    description text,
    image       varchar(255)
);

alter table user_details
    owner to akxlgzthnxmupr;

create unique index user_details_id_uindex
    on user_details (id);

create table aircraft_images
(
    id          serial
        constraint aircraft_images_pk
            primary key,
    image       varchar(255) not null,
    id_aircraft integer      not null
        constraint aircraft__aircraft_images___fk
            references aircrafts
            on delete cascade
);

alter table aircraft_images
    owner to akxlgzthnxmupr;

create unique index aircraft_images_id_uindex
    on aircraft_images (id);

create table user_types
(
    id   serial
        constraint user_types_pk
            primary key,
    type varchar(255) not null
);

alter table user_types
    owner to akxlgzthnxmupr;

create table users
(
    id              integer default nextval('user_id_seq'::regclass) not null
        constraint user_pk
            primary key,
    name            varchar(100)                                     not null,
    surname         varchar(100)                                     not null,
    email           varchar(255)                                     not null,
    password        varchar(255)                                     not null,
    id_user_details integer
        constraint user_details__users___fk
            references user_details
            on update cascade on delete cascade,
    id_user_type    integer default 1                                not null
        constraint user_types__users___fk
            references user_types
            on update set default on delete set default
);

alter table users
    owner to akxlgzthnxmupr;

create table offers
(
    id          serial
        constraint offers_pk
            primary key,
    title       varchar(255) not null,
    description varchar(255),
    price       integer      not null,
    id_aircraft integer
        constraint aircraft_offers___fk
            references aircrafts
            on update cascade on delete cascade,
    created_at  date         not null,
    created_by  integer      not null
        constraint user_offers___fk
            references users
            on update cascade on delete cascade,
    id_address  integer
        constraint addresses_offers___fk
            references addresses
);

alter table offers
    owner to akxlgzthnxmupr;

create unique index offers_id_uindex
    on offers (id);

create unique index user_email_uindex
    on users (email);

create unique index user_id_uindex
    on users (id);

create unique index user_types_id_uindex
    on user_types (id);

create unique index user_types_type_uindex
    on user_types (type);

