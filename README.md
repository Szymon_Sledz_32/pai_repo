#WDPAI
**Projekt aplikacji wypożyczania samolotów - Sky-renters**

---

Aplikacja zawiera:

-mechanizm logowania i sesji

-rozpoznawanie uprawnień użytkowników

-wylogowanie - usunięcie sesji

-możliwość edycji profilu użytkownika

-możliwość dodania/usunięcia własnej oferty

-możliwość przeglądania wszystkich dostępnych ofert

---
